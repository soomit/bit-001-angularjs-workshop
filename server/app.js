'use strict';

var express = require('express');
var logger = require('morgan');
var bodyParser = require('body-parser');

var players = require('./routes/players');

var app = express();

// Enable CORS from corsUrl:
app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Credentials', true);
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
  res.header('Access-Control-Allow-Headers', 'Accept, Authorization, Content-Type, Origin, X-Requested-With');
  next();
});

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use('/players', players);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers
// development error handler
// will print stacktrace
app.use(function(err, req, res, next) {
  /* jshint unused: false */
  res.status(err.status || 500);
  res.json({ message: err.message, error: err});
});

module.exports = app;
