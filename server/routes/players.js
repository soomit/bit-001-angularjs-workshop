'use strict';

var express = require('express');
var fs = require('fs');
var router = express.Router();

//in-memory db ;-)
var idGenerator = 1;
var db = {};

function save(player) {
  player.id = idGenerator++;
  db[player.id] = player;
  return player;
}

function randomplayers() {
  var data = fs.readFileSync(__dirname + '/../data/randomplayers.json');
  return JSON.parse(data);
}

router.param('id', function (req, res, next, id) {
  var player = db[id];
  if (!player) {
    var err = new Error('no such player: ' + id);
    err.status = 404;
    next(err);
    return;
  }
  req.player = player;
  next();
});

router.get('/generate', function(req, res) {
  randomplayers().forEach(function(player) {
    save(player);
  });
  res.json({success: true, msg: 'Generated 100 players'});
});

/* GET players listing. */
router.get('/', function(req, res) {
  var players = [];
  for (var id in db) {
    var player = db[id];
    players.push(player);
  }
  res.json(players);
});

router.post('/', function(req, res) {
  var player = req.body;
  res.json(save(player));
});

router.put('/:id', function(req, res) {
  var id = req.player.id;
  var update = req.body;
  for (var attr in update) {
    req.player[attr] = update[attr];
  }
  req.player.id = id; //make sure id is not overriden;
  res.json(req.player);
});

router.get('/:id', function(req, res) {
  res.json(req.player);
});

router.delete('/:id', function(req, res) {
  delete db[req.player.id];
  res.json(req.player);
});

module.exports = router;
