// Generated on 2015-05-29 using generator-angular 0.10.0
'use strict';

// # Globbing
// for performance reasons we're only matching one level down:
// 'test/spec/{,*/}*.js'
// use this if you want to recursively match all subfolders:
// 'test/spec/**/*.js'

module.exports = function (grunt) {

  // Load grunt tasks automatically
  require('load-grunt-tasks')(grunt);

  // Time how long tasks take. Can help when optimizing build times
  require('time-grunt')(grunt);

  // Define the configuration for all the tasks
  grunt.initConfig({

    // Make sure code styles are up to par and there are no obvious mistakes
    jshint: {
      options: {
        jshintrc: '.jshintrc',
        reporter: require('jshint-stylish')
      },
      all: {
        src: [
          'Gruntfile.js',
          'app/**/*.js'
        ]
      }
    },

    browserSync: {
      dev: {
        bsFiles: {
          src : [
            'app/*.css',
            'app/**/*.html',
            'app/**/*.js'
          ]
        },
        options: {
          server: {
            baseDir: 'app',
            routes: {
              '/vendor': 'vendor'
            }
          }
        }
      }
    },

    // Test settings
    karma: {
      unit: {
        configFile: 'karma.conf.js',
        singleRun: true
      }
    },

    // E2E Tests
    protractor: {
      options: {
        keepAlive: true, // If false, the grunt process stops when the test fails.
        noColor: false, // If true, protractor will not use colors in its output.
      },
      all: {   // Grunt requires at least one target to run so you can simply put 'all: {}' here too.
        options: {
          configFile: 'protractor.conf.js', // Target-specific config file
          args: {} // Target-specific arguments
        }
      },
    }
  });

  grunt.registerTask('serve', ['newer:jshint', 'browserSync']);

  grunt.registerTask('test', [ 'newer:jshint', 'karma']);

  grunt.registerTask('e2e-test', [ 'newer:jshint', 'protractor']);

};
