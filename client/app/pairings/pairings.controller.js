'use strict';

angular.module('tourney.pairings')

.controller('PairingsCtrl', function($scope, tournamentService, errorHandler) {

  $scope.pairings = [];

  tournamentService.getPairings()
    .then(function (data) {
      $scope.pairings = data;
    });

  $scope.generatePairings = function () {
    tournamentService.generatePairings()
      .catch(function(e) {
        errorHandler.addError(e);
      });
  };
});
