'use strict';

angular.module('tourney.pairings', ['ui.router', 'tourney.tournament'])

//define our routes/states
.config(function($stateProvider) {
  $stateProvider
    .state('pairings', {
      url: '/pairings',
      templateUrl: 'pairings/pairings.tpl.html',
      controller: 'PairingsCtrl'
    });
});
