'use strict';

angular.module('tourney.home', [
  'ui.router',
  'tourney.tournament'
])

//define our routes/states
.config(function($stateProvider) {
  $stateProvider
    .state('home', {
      url: '/home',
      templateUrl: 'home/home.tpl.html',
      controller: 'HomeCtrl'
    });
});
