'use strict';

angular.module('tourney.home')

.controller('HomeCtrl', function($scope, tournamentService) {

  $scope.hasPlayers = false;

  tournamentService.hasPlayers()
    .then(function (data) {
      $scope.hasPlayers = data;
    });
});
