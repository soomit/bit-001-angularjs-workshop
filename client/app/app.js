'use strict';

// Declare app level module which depends on views, and components
angular.module('tourney', [
  'ui.router',
  'tourney.navbar',
  'tourney.capitalize',
  'tourney.home',
  'tourney.players',
  'tourney.pairings',
  'tourney.errorhandler'
])

.config(function($urlRouterProvider) {
  /* jshint unused: false */
  // For any unmatched url, redirect to /home
  $urlRouterProvider.otherwise('/home');
});
