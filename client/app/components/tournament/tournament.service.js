'use strict';

angular.module('tourney.tournament')

.factory('tournamentService', function($http, $q, appErrors) {
  //private
  var apiUrl = 'http://localhost:5000';
  var pairings = [];

  /**
   * This service returns always a Promise
   */

  return {
    hasPlayers: function () {
      /*
       * a real world server would expose a "count" endpoint.
       * This implementation is inefficient.
       */
      return $http.get(apiUrl + '/players')
        .then(function (response) {
          return response.data.length > 0;
        });
    },

    getPlayers: function () {
      return $http.get(apiUrl + '/players');
    },

    getPlayer: function (id) {
      return $http.get(apiUrl + '/players/' + id);
    },

    updatePlayer: function (player) {
      return $http.put(apiUrl + '/players/' + player.id, player);
    },

    addPlayer: function (player) {
      return $http.post(apiUrl + '/players/', player);
    },

    removePlayer: function (id) {
      return $http.delete(apiUrl + '/players/' + id);
    },

    getPairings: function() {
      return $q.when(pairings);
    },

    generatePairings: function () {
      return this.getPlayers()
        .then(function (response) {
          var players = response.data;
          //validation
          if (players.length === 0 || players.length % 2 !== 0) {
            return $q.reject(new Error(appErrors['tournament.generate_pairings']));
          }

          //random sort
          players.sort(function() {
            return Math.random() > 0.5 ? 1 : -1;
          });

          for (var i = 0; i < players.length/2; i++) {
            pairings[i] = [players[i].name, players[(players.length/2) + i].name];
          }
        });
    }
  };
});
