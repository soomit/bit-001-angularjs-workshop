'use strict';

var service, $httpBackend;

var apiUrl = 'http://localhost:5000';

describe('tournamentService', function () {

  beforeEach(module('tourney.tournament'));
  beforeEach(module('tourney.errorhandler'));

  beforeEach(inject(function ($injector, _$httpBackend_) {
    service = $injector.get('tournamentService');
    $httpBackend = _$httpBackend_;
    $httpBackend.whenGET(apiUrl + '/players').respond([{ name: 'test-player' }]);
  }));

  afterEach(function () {
    $httpBackend.flush();
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });

  it('should get players', function () {
    $httpBackend.expectGET(apiUrl + '/players');

    service.getPlayers().success(function(data) {
      expect(data.length).toEqual(1);
      expect(data[0].name).toEqual('test-player');
    });
  });
});