'use strict';

angular.module('tourney.capitalize')
.filter('capitalize', function () {
  return function (input) {
    var output = input;
    //first char to upper case
    if (input.length > 0) {
      output = input[0].toUpperCase() + input.slice(1);
    }
    return output;
  };
});
