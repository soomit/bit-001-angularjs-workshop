'use strict';

angular.module('tourney.errorhandler')

.factory('errorHandler', function($log) {
  return {

    errors: [],

    getErrors: function() {
      return this.errors;
    },

    hasErrors: function() {
      return this.errors.length > 0;
    },

    addError: function(err) {
      if (!this._shouldAddError(err)) {
        $log.debug('skipping duplicate error: ', err);
        return;
      }
      this.errors.push(err);
    },

    dismissError: function(idx) {
      this.errors.splice(idx, 1);
    },

    //private
    _shouldAddError: function(err) {
      if (!(err && err instanceof Error)) {
        return false;
      }
      return !this.errors.some(function(addedError) {
        return addedError.message === err.message;
      });
    }
  };
});
