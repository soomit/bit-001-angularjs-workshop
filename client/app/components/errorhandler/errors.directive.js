'use strict';

angular.module('tourney.errorhandler')

.controller('ErrorsCtrl', function($scope, errorHandler) {
  $scope.errors = errorHandler.getErrors();
  $scope.dismiss = errorHandler.dismissError;
})

.directive('errors', function() {
  return {
    templateUrl: 'components/errorhandler/errors.tpl.html',
  };
});
