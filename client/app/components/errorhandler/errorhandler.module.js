'use strict';

angular.module('tourney.errorhandler', [])

.constant('appErrors', {
  'tournament.generate_pairings': 'Could not generate pairings. Make sure to have an even number of players'
})

.constant('httpErrors', {
  0: 'Ooops. Could not connect to backend. Please try again later',
  500: 'Oh snap. An unexpected error occured. We try to fix it as soon as possible, thanks for your patience.'
})

.config(function($httpProvider) {
  $httpProvider.interceptors.push(function($q, $log, httpErrors, errorHandler){
    return {
      'responseError': function(rejection) {
        var msg = httpErrors[rejection.status];
        if (msg) {
          errorHandler.addError(new Error(msg));
        }
        return $q.reject(rejection);
      }
    };
  });
});
