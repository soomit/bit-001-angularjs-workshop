'use strict';

angular.module('tourney.navigation')

.controller('NavigationCtrl', function($scope, tournamentService) {
  $scope.hasPairings = tournamentService.hasPairings;
});
