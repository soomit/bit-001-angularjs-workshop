'use strict';

describe('PlayersCreateCtrl', function(){

  var scope, controller, tournamentSpy;

  beforeEach(module('tourney.templates'));
  beforeEach(module('tourney.players'));
  beforeEach(module('tourney.errorhandler'));

  beforeEach(inject(function($rootScope, $templateCache, $compile, $controller, tournamentService) {
    scope = $rootScope.$new();
    tournamentSpy = tournamentService;
    spyOn(tournamentSpy, 'addPlayer');

    controller = $controller('PlayersCreateCtrl', {
      $scope: scope
    });

    var tpl = $templateCache.get('players/players.create.tpl.html');
    expect(tpl).toBeTruthy();
    $compile(angular.element(tpl).find('form'))(scope);
    scope.$apply();
  }));

  it('should define available genders', function() {
    expect(angular.isArray(scope.genders)).toBeTruthy();
  });

  it('should add player when validation passes', function() {
    //GIVEN
    expect(scope.form).toBeTruthy();
    expect(scope.form.$valid).toBe(false);
    scope.player = {
      name: 'Test',
      email: 'test@test.ch',
      gender: 'male'
    };
    scope.$apply();

    //WHEN
    scope.addPlayer();

    //THEN
    expect(tournamentSpy.addPlayer).toHaveBeenCalled();
    expect(scope.form.$pristine).toBe(true);
  });
});
