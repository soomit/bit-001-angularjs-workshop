'use strict';

angular.module('tourney.players', [
  'ui.router',
  'tourney.tournament',
  'tourney.capitalize'
])

//define our routes/states
.config(function($stateProvider, $urlRouterProvider) {

  $urlRouterProvider.when('/players', '/players/');

  $stateProvider
    .state('players', { url: '/players', templateUrl: 'players/players.tpl.html'})
    .state('players.list', {url: '/', templateUrl: 'players/players.list.tpl.html', controller: 'PlayersListCtrl'})
    .state('players.create', {url: '/create', templateUrl: 'players/players.create.tpl.html', controller: 'PlayersCreateCtrl'})
    .state('players.edit', {url: '/:playerId', templateUrl: 'players/players.edit.tpl.html', controller: 'PlayersEditCtrl'});
})

.constant('GENDERS', ['male', 'female']);
