'use strict';

angular.module('tourney.players')

.controller('PlayersCreateCtrl', function ($scope, tournamentService, GENDERS) {

  $scope.genders = GENDERS;

  $scope.newPlayer = function() {
    return {
      name: '',
      email: '',
      gender: 'male'
    };
  };

  $scope.player = $scope.newPlayer();

  $scope.addPlayer = function() {
    //$scope.form is implicitly given because of the forms name attribute <form name="form" ..>
    if (!$scope.form.$valid) {
      return;
    }

    tournamentService.addPlayer($scope.player);

    //reset or navigate somewhere
    $scope.player = $scope.newPlayer();
    $scope.form.$setPristine(); //reset form to its pristine state

    //TODO (optional) notification service
  };
});
