'use strict';

angular.module('tourney.players')

.controller('PlayersEditCtrl', function ($scope, $state, $stateParams, tournamentService, GENDERS) {

  $scope.genders = GENDERS;
  tournamentService.getPlayer($stateParams.playerId)
    .success(function (data) {
      $scope.player = data;
    });

  $scope.save = function () {
    tournamentService.updatePlayer($scope.player)
      .success(function () {
        $state.go('^.list');
      });
  };
});
