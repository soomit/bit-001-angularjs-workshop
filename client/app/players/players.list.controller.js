'use strict';

angular.module('tourney.players')

.controller('PlayersListCtrl', function ($scope, tournamentService) {

  $scope.players = [];

  tournamentService.getPlayers()
    .success(function (data) {
      $scope.players = data;
    });

  $scope.remove = function (idx) {
    var player = $scope.players[idx];
    tournamentService.removePlayer(player.id)
      .success(function() {
        $scope.players.splice(idx, 1);
      });
  };
});
