'use strict';

describe('tourney homepage', function () {
  var page;

  beforeEach(function () {
    page = require('./home.po');
    page.get();
  });

  it('should have a correct page header', function() {
    expect(page.header.getText()).toBe('Welcome!');
  });

  it('should show a "Create Players" button', function() {
    expect(page.createPlayersButton.isPresent()).toBe(true);
  });

  it('should navigate to "players" view', function() {
    page.createPlayersButton.click();
    expect(browser.getCurrentUrl()).toMatch(/\/players/);
  });

});
