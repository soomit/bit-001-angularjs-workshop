/**
 * This file uses the Page Object pattern to define the main page for tests
 * https://docs.google.com/presentation/d/1B6manhG0zEXkC-H-tPo2vwU06JhL8w9-XCF9oehXzAQ
 */

'use strict';

var PlayerCreatePage = function() {
  this.header = element(by.css('.page-header'));
  this.nameInput = element(by.model('player.name'));
  this.emailInput = element(by.model('player.email'));
  this.addPlayerButton = element(by.cssContainingText('.btn', 'Add player'));
  this.backButton = element(by.cssContainingText('.btn', 'Back to list'));
  this.get = function() {
    browser.get('/#players/create');
  };
  this.setName = function(name) {
    this.nameInput.sendKeys(name);
  };
  this.setEmail = function(email) {
    this.emailInput.sendKeys(email);
  };
  this.addPlayer = function() {
    this.addPlayerButton.click();
  };
  this.backToList = function() {
    this.backButton.click();
  };
};

module.exports = new PlayerCreatePage();
