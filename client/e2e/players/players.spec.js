'use strict';

describe('tourney "players" pages', function () {

  describe('player "list"', function() {
    var page;
    beforeEach(function () {
      page = require('./players.list.po');
      page.get();
    });

    it('should have a correct page header', function() {
      expect(page.header.getText()).toBe('Players');
    });

    it('should show "no players yet"', function() {
      expect(page.noPlayersText.isPresent()).toBe(true);
      expect(page.noPlayersText.getText()).toBe('No players yet ;-(');
    });

    it('should navigate to "create player"', function() {
      page.createPlayersButton.click();
      expect(browser.getCurrentUrl()).toMatch(/\/players\/create/);
    });
  });

  describe('player "create"', function() {

    var page;
    beforeEach(function () {
      page = require('./players.create.po');
      page.get();
    });

    it('should create player "Cedric" and clear input fields', function() {
      page.setName('Cédric');
      page.setEmail('c@soom-it.ch');
      expect(page.emailInput.getAttribute('value')).toBe('c@soom-it.ch');
      page.addPlayer();
      expect(page.emailInput.getAttribute('value')).toBe('');
      expect(page.nameInput.getAttribute('value')).toBe('');
    });

    it('should navigate "back to list"', function() {
      page.backToList();
      expect(browser.getCurrentUrl()).toMatch(/\/players/);
    });

  });

  describe('player "list" delete', function() {
    var page;
    beforeEach(function () {
      page = require('./players.list.po');
      page.get();
    });

    it('should show 1 player', function() {
      expect(page.playerList.count()).toBe(1);
      expect(page.playerList.get(0).$('td:nth-child(1)').getText()).toBe('Cédric');
    });

    it('should delete player', function() {
      expect(page.getDeleteButton(0).isDisplayed()).toBe(true);
      page.getDeleteButton(0).click();
    });

    it('should show "no players yet"', function() {
      expect(page.noPlayersText.isPresent()).toBe(true);
      expect(page.noPlayersText.getText()).toBe('No players yet ;-(');
    });

  });
});
