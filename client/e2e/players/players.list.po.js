/**
 * This file uses the Page Object pattern to define the main page for tests
 * https://docs.google.com/presentation/d/1B6manhG0zEXkC-H-tPo2vwU06JhL8w9-XCF9oehXzAQ
 */

'use strict';

var PlayerListPage = function() {
  this.header = element(by.css('.page-header'));
  this.createPlayersButton = element(by.cssContainingText('.btn', 'Create new player'));
  this.noPlayersText = element(by.tagName('tbody'));
  this.playerList = element.all(by.repeater('player in players'));
  this.get = function() {
    browser.get('/#players');
  };
  this.getDeleteButton = function(idx) {
    return this.playerList.get(idx).element(by.cssContainingText('.btn', 'Remove'));
  };
};

module.exports = new PlayerListPage();
