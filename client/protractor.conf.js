'use strict';

// An example configuration file.
exports.config = {
  // The address of a running selenium server. Can also be a remote one
  seleniumAddress: 'http://localhost:4444/wd/hub',

  // Base url of our application (typically an integration system)
  baseUrl: 'http://localhost:3000',

  // Direct connect (dont use selenium server)
  // works only for: 'chrome' & 'firefox'
  directConnect: true,

  // Capabilities to be passed to the webdriver instance.
  capabilities: {
    'browserName': 'chrome'
  },

  // Best Practice: Seperate your tests in suites
  suites: {
    homepage: ['e2e/home/*.spec.js'],
    players: ['e2e/players/*.spec.js']
  },

  // Options to be passed to Jasmine-node.
  jasmineNodeOpts: {
    showColors: true,
    defaultTimeoutInterval: 30000
  }
};
