/*!
 * ObliqueUI - v1.2.0-RC8
 * http://vmjeap10i04.bfi.admin.ch/eui/oblique-ui/
 * Copyright (c) 2015 Federal Office of Information Technology, Systems and Telecommunication FOITT (http://www.bit.admin.ch/)
 */
/* ========================================================================
 * ObliqueUI Framework: main.js v1.2.0-RC8
 * http://vmjeap10i04.bfi.admin.ch/eui/oblique-ui/#main
 * ======================================================================== */

if (typeof Modernizr === 'undefined') {
	throw new Error('ObliqueUI\'s JavaScript requires Modernizr');
}

Modernizr.load([
	// Presentational polyfills
	{
		test : Modernizr.flexbox,
		nope : {
			'matchHeight' : ($('[data-obliqueui-path]').length ? $('[data-obliqueui-path]').data('obliqueui-path') : '') + 'support/jquery.matchHeight-min.js'
		},
		callback: {
			'matchHeight': function (url, result, key) {
				$('.app-wrapper, #content, .offcanvas, .offcanvas > div').matchHeight(); // .app-wrapper, #content, .offcanvas,

				$(window).scroll(function(){
					//$.fn.matchHeight._update();
				});
			}
		}
	}
]);

$(function(){
	'use strict';

	// Tooltips
	// ---------------------------------

	// Data-API initialization:
	$('[data-toggle="tooltip"]').tooltip();

	// Scoped initialization (enables tooltips on elements that specify a `title` attribute):
	$('.has-tooltips').find("a[title], button[title], span[title], label[title], img[title]").tooltip();
});

/* ========================================================================
 * ObliqueUI Framework: activable.js v1.2.0-RC8
 * http://vmjeap10i04.bfi.admin.ch/eui/oblique-ui/plugins/activable.html
 * ======================================================================== */
+function ($) {
	'use strict';

	// ACTIVABLE CLASS DEFINITION
	// =========================

	var selector = '[data-provides~="activable"]';

	var Activable = function (element, options) {
		this.$element   = $(element);
		this.options    = options;
	};

	Activable.prototype.toggle =  function (e) {
		var $this = $(this);
		if(getContainer($this).hasClass('open')){
			$this.activable('deactivate');
		} else {
			$this.activable('activate');
		}
	};

	Activable.prototype.activate =  function (e) {
		var $this = $(this);
		var $parent  = getParent($this);

		var relatedTarget = { relatedTarget: this };
		$parent.trigger(e = $.Event('activate.oblique-ui.activable', relatedTarget));
		if (e.isDefaultPrevented()) {return;}

		$parent.find('.active > a').not($this).activable('deactivate');

		$this.activable('enter');
		$this.data('activated', true);
		getContainer($this).addClass('open');

		$parent.trigger('activated.oblique-ui.activable', relatedTarget);
	};

	Activable.prototype.deactivate =  function (e) {
		var $this = $(this);
		$this.data('activated', false);
		getContainer($this).removeClass('open');
		$this.activable('leave');
	};

	Activable.prototype.enter =  function (e) {
		$(this).parent('li').addClass('active');
	};

	Activable.prototype.leave =  function (e) {
		var $this = $(this);
		!$this.data('activated') && $this.parent('li').removeClass('active');
	};

	function getContainer($this) {
		return $this.parent('li').length ? $this.parent('li') : getParent($this);
	}

	function getParent($this) {
		var $parent = $this.closest(selector);
		$parent = $parent.length && $parent.data('parent') ? $parent.closest($parent.data('parent')) : $parent;
		return $parent && $parent.length ? $parent : $this;
	}

	function onHashChange() {
		if(document.location.hash) {
			try {
				$(selector + ' a[href$=' + document.location.hash + ']').each(Activable.prototype.activate);
			} catch(e) {
				/* Ignore: Syntax error, unrecognized expression */
			}
		}
	}

	// ACTIVABLE PLUGIN DEFINITION
	// ==========================

	var old = $.fn.activable;

	$.fn.activable = function (option) {
		return this.each(function () {
			var $this   = $(this);
			var data    = $this.data('oblique-ui.activable');

			if (!data) {
				$this.data('oblique-ui.activable', (data = new Activable(this)));
			}
			if (typeof option === 'string') {
				data[option].call($this);
			}
		});
	};

	$.fn.activable.Constructor = Activable;


	// ACTIVABLE NO CONFLICT
	// ====================

	$.fn.activable.noConflict = function () {
		$.fn.activable = old;
		return this;
	};


	// ACTIVABLE DATA-API
	// =================
	var activableSelector = 'a' + selector + ', ' + selector + ' > li > a';
	$(document)
		.on('click.oblique-ui.activable.data-api', activableSelector, Activable.prototype.toggle)
		//.on('blur.oblique-ui.activable.data-api', activableSelector, Activable.prototype.deactivate)
		.on('mouseenter.oblique-ui.activable.data-api', activableSelector, Activable.prototype.enter)
		.on('mouseleave.oblique-ui.activable.data-api', activableSelector, Activable.prototype.leave)
		.on('ready', onHashChange);
	$(window)
		.on('hashchange', onHashChange);
}(jQuery);

/* ========================================================================
 * ObliqueUI Framework: column-layout.js v1.2.0-RC8
 * http://vmjeap10i04.bfi.admin.ch/eui/oblique-ui/plugins/column-layout.html
 * ======================================================================== */
+function ($) {
	'use strict';

	// COLUMN-LAYOUT CLASS DEFINITION
	// =========================

	var ColumnLayout = function (element, options) {
		this.$element   = $(element);
		this.options    = options;
	};

	ColumnLayout.prototype.toggle =  function (e, columnKey) {
		var $this   = $(this);
		var column  = typeof columnKey === 'string' ? columnKey : $this.attr('data-column-toggle');
		var $column = $('[data-column="' + column + '"]');
		var $parent = $column.parents('[data-provides="column-layout"]');

		var relatedTarget = { relatedTarget: this, column : $column };
		$parent.trigger(e = $.Event('toggle.oblique-ui.column-layout', relatedTarget));

		if (e.isDefaultPrevented()) {return;}

		$parent
			.toggleClass('column-expanded-' + column)
			.trigger('toggled.oblique-ui.column-layout', relatedTarget);

		return false;
	};

	// COLUMN-LAYOUT PLUGIN DEFINITION
	// ==========================

	var old = $.fn.columnLayout;

	$.fn.columnLayout = function (column) {
		return this.each(function () {
			var $this   = $(this);
			var data    = $this.data('oblique-ui.column-layout');

			if (!data) {
				$this.data('oblique-ui.column-layout', (data = new ColumnLayout(this)));
			}
			if (typeof column === 'string') {
				data.toggle(null, column);
			}
		});
	};

	$.fn.columnLayout.Constructor = ColumnLayout;


	// COLUMN-LAYOUT NO CONFLICT
	// ====================

	$.fn.columnLayout.noConflict = function () {
		$.fn.columnLayout = old;
		return this;
	};


	// COLUMN-LAYOUT DATA-API
	// =================

	$(document).on('click.oblique-ui.column-layout.data-api', '[data-column-toggle]', ColumnLayout.prototype.toggle);

	$(window).on('load', function () {
		$('[data-provides="column-layout"]').each(function () {
			var $columnLayout = $(this);
			$columnLayout.columnLayout($columnLayout.data());
		});
	});

}(jQuery);

/* ========================================================================
 * ObliqueUI Framework: inner-navigation.js v1.2.0-RC8
 * http://vmjeap10i04.bfi.admin.ch/eui/oblique-ui/plugins/inner-navigation.html
 * ======================================================================== */
+function ($) {
	'use strict';

	// INNER-NAVIGATION CLASS DEFINITION
	// =========================

	var selector = '[data-provides~="inner-navigation"]';

	var InnerNavigation = function (element, options) {
		this.$element   = $(element);
		this.options    = options;
	};

	InnerNavigation.prototype.scrollTo =  function (e) {
		var $this = $(this);

		// Retrieve hash:
		var hash = this.hash;
		var anchor = $(hash);

		if(anchor.length) {
			// Prevent default anchor click behavior:
			e && e.preventDefault();

			var $parent = getParent($this);
			var relatedTarget = { relatedTarget: this, hash: hash };
			$parent.trigger(e = $.Event('scroll.oblique-ui.inner-navigation', relatedTarget));

			if (e.isDefaultPrevented()) {
				return;
			}

			// Retrieve any extra top offset:
			var topOffset = anchor.offset().top;
			var additionalTopOffsets = $('[data-offset-top]').map(function() {
				var top = $(this).data('offset-top');
				return topOffset > top ? top : 0;
			}).get();

			var additionalTopOffset = additionalTopOffsets.length ? Math.max.apply(null, additionalTopOffsets) : 0;
			var scrollTop = topOffset - additionalTopOffset;

			// Animate:
			$('html, body').animate({
				scrollTop: scrollTop
			}, 300, function () {
				// Restore default behaviour:
				window.location.hash = hash;
			});

			$parent.trigger('scrolled.oblique-ui.inner-navigation', relatedTarget);
		}
	};

	function getParent($this) {
		var $parent = $this.closest(selector);
		$parent = $parent.length && $parent.data('parent') ? $parent.closest($parent.data('parent')) : $parent;
		return $parent && $parent.length ? $parent : $this;
	}

	// INNER-NAVIGATION PLUGIN DEFINITION
	// ==========================

	var old = $.fn.innerNavigation;

	$.fn.innerNavigation = function () {
		return this.each(function () {
			var $this   = $(this);
			var data    = $this.data('oblique-ui.inner-navigation');

			if (!data) {
				$this.data('oblique-ui.inner-navigation', (data = new InnerNavigation(this)));
			}
			data.scrollTo.call(this);
		});
	};

	$.fn.innerNavigation.Constructor = InnerNavigation;


	// INNER-NAVIGATION NO CONFLICT
	// ====================

	$.fn.innerNavigation.noConflict = function () {
		$.fn.innerNavigation = old;
		return this;
	};


	// INNER-NAVIGATION DATA-API
	// =================

	$(document).on(
		'click.oblique-ui.inner-navigation.data-api',
		selector + ' li > a, a' + selector,
		InnerNavigation.prototype.scrollTo
	);

}(jQuery);

/**
 * ObliqueUI Offcanvas plugin
 *
 * TODO: improve offcanvas dual states (within desktop & collapsed grid)
 */
+function ($) {
	'use strict';

	// OFFCANVAS CLASS DEFINITION
	// =========================

	var selector = '[data-toggle~="offcanvas"]',
		transitionDuration = 600;

	var Offcanvas = function (element, options) {
		this.$element   = $(element);
		this.options    = options;

		this.transitioning = null;
	};

	Offcanvas.DEFAULTS = {
		collapseLimit : 991 // Bootstrap's SM to MD breakpoint
	};

	Offcanvas.prototype.show = function () {
		if (this.transitioning || this.isActive()){
			return;
		}

		var self = this; // Fixes $.proxy issues on IE9

		var startEvent = $.Event('show.oblique-ui.offcanvas');
		this.$element.trigger(startEvent);
		if (startEvent.isDefaultPrevented()){
			return;
		}

		this.transitioning = 1;
		this.$backdrop = $('<div class="modal-backdrop offcanvas-backdrop fade" />');

		this.$element.addClass('offcanvas-in');

		if(this.collapsedMode()){
			this.$backdrop.appendTo(this.$element);
			this.$backdrop[0].offsetWidth; // Important: force reflow
			this.$backdrop.addClass('in');
			this.$element.addClass('offcanvas-open');
		}

		var complete = function () {
			self.$backdrop.on('click.oblique-ui.offcanvas', $.proxy(self.hide, self));
			self.transitioning = 0;
			self.$element.trigger('shown.oblique-ui.offcanvas');
		};

		if ($.support.transition) {
			this.$element
				.one($.support.transition.end, complete)
				.emulateTransitionEnd(transitionDuration);
		} else {
			complete();
		}
	};

	Offcanvas.prototype.hide = function () {
		if (this.transitioning || !this.isActive()){
			return;
		}

		var self = this; // Fixes $.proxy issues on IE9

		var startEvent = $.Event('hide.oblique-ui.offcanvas');
		this.$element.trigger(startEvent);
		if (startEvent.isDefaultPrevented()){
			return;
		}

		this.transitioning = 1;

		this.$backdrop && this.$backdrop.removeClass('in');
		this.$element.removeClass('offcanvas-in');
		this.$element.removeClass('offcanvas-open');

		var complete = function () {
			if(self.$backdrop){
				self.$backdrop.remove();
				self.$backdrop = null;
			}
			self.transitioning = 0;
			self.$element.trigger('hidden.oblique-ui.offcanvas');
		};

		if ($.support.transition) {
			this.$element
				.one($.support.transition.end, complete)
				.emulateTransitionEnd(transitionDuration);
		} else {
			complete();
		}
	};

	Offcanvas.prototype.toggle = function () {
		this[this.isActive() ? 'hide' : 'show']();
	};

	Offcanvas.prototype.isActive = function () {
		return this.$element.hasClass(this.collapsedMode() ? 'offcanvas-open' : 'offcanvas-in');
	};

	Offcanvas.prototype.collapsedMode = function() {
		var overflowY = $('body').css('overflow-y');
		$('body').css({'overflow-y' : "hidden"});
		var width = $(window).width();
		$('body').css({'overflow-y' : overflowY});

		return width <= this.options.collapseLimit;
	};

	// OFFCANVAS PLUGIN DEFINITION
	// ==========================

	function Plugin(option, args) {
		return this.each(function () {
			var $this   = $(this);
			var data    = $this.data('oblique-ui.offcanvas');
			var options = $.extend({}, Offcanvas.DEFAULTS, $this.data(), typeof option === 'object' && option);

			if (!data) {
				$this.data('oblique-ui.offcanvas', (data = new Offcanvas(this, options)));
			}
			if (typeof option === 'string') {
				data[option].apply(data, args);
			}
		});
	}

	var old = $.fn.offcanvas;

	$.fn.offcanvas                = Plugin;
	$.fn.offcanvas.Constructor    = Offcanvas;


	// OFFCANVAS NO CONFLICT
	// ====================

	$.fn.offcanvas.noConflict = function () {
		$.fn.offcanvas = old;
		return this;
	};


	// OFFCANVAS DATA-API
	// =================

	$(document).on('click.oblique-ui.offcanvas.data-api', selector, function(e){
		var $this   = $(this);
		var href    = null;
		var target  = $this.attr('data-target') || e.preventDefault() || (href = $this.attr('href')) && href.replace(/.*(?=#[^\s]+$)/, ''); // strip for ie7
		var $target = $(target);

		Plugin.call($target.length ? $target : $this.closest('.offcanvas'), 'toggle');
	});

	$(document).keyup(function(e) {
		if (e.keyCode === 27) { // ESC
			Plugin.call($('.offcanvas.offcanvas-open').last(), 'hide');
		}
	});

}(jQuery);

/**
 * ObliqueUI Strate plugin
 *
 * Inspired from:
 *  - http://tympanus.net/Development/PageTransitions/
 *  - http://tympanus.net/Tutorials/CSS3PageTransitions/index3.html
 *  - http://multi-level-push-menu.make.rs/
 */
+function ($) {
	'use strict';

	// STRATE CLASS DEFINITION
	// =========================

	var selector = '[data-toggle~="strate"]',
		inClass = 'opening',
		outClass = 'closing',
		transitionDuration = 600;

	var Strate = function (element, options) {
		this.$element   = $(element);
		this.options    = options;

		this.transitioning = null;
	};

	Strate.DEFAULTS = {};

	Strate.prototype.show = function () {
		if (this.transitioning || this.$element.hasClass('in')){
			return;
		}

		var self = this; // Fixes $.proxy issues on IE9

		var startEvent = $.Event('show.oblique-ui.strate');
		this.$element.trigger(startEvent);
		if (startEvent.isDefaultPrevented()){
			return;
		}

		this.transitioning = 1;

		this.$backdrop = $('<div class="modal-backdrop fade" />').insertAfter(this.$element);
		this.$backdrop[0].offsetWidth; // Important: force reflow
		this.$backdrop.addClass('in');
		this.$element.addClass(inClass);

		var complete = function () {
			self.$backdrop.on('click.oblique-ui.strate', $.proxy(self.hide, self));
			self.$element.removeClass(inClass).addClass('in');
			self.transitioning = 0;
			self.$element.trigger('shown.oblique-ui.strate');
		};

		if ($.support.transition) {
			this.$element
				.one($.support.transition.end, complete)
				.emulateTransitionEnd(transitionDuration);
		} else {
			complete();
		}
	};

	Strate.prototype.hide = function () {
		if (this.transitioning || !this.$element.hasClass('in')){
			return;
		}

		var self = this; // Fixes $.proxy issues on IE9

		var startEvent = $.Event('hide.oblique-ui.strate');
		this.$element.trigger(startEvent);
		if (startEvent.isDefaultPrevented()){
			return;
		}

		this.transitioning = 1;

		this.$backdrop.removeClass('in');
		this.$element.addClass(outClass).removeClass('in');

		var complete = function () {
			self.$element.removeClass(outClass);
			self.$backdrop.remove();
			self.$backdrop = null;
			self.transitioning = 0;

			self.$element.closest('.strate-item.open').removeClass('open');

			self.$element.trigger('hidden.oblique-ui.strate');
		};

		if ($.support.transition) {
			this.$element
				.one($.support.transition.end, complete)
				.emulateTransitionEnd(transitionDuration);
		} else {
			complete();
		}
	};

	Strate.prototype.toggle = function () {
		this[this.$element.hasClass('in') ? 'hide' : 'show']();
	};

	// STRATE PLUGIN DEFINITION
	// ==========================

	function Plugin(option, args) {
		return this.each(function () {
			var $this   = $(this);
			var data    = $this.data('oblique-ui.strate');
			var options = $.extend({}, Strate.DEFAULTS, $this.data(), typeof option === 'object' && option);

			if (!data) {
				$this.data('oblique-ui.strate', (data = new Strate(this, options)));
			}
			if (typeof option === 'string') {
				data[option].apply(data, args);
			}
		});
	}

	var old = $.fn.strate;

	$.fn.strate                = Plugin;
	$.fn.strate.Constructor    = Strate;


	// STRATE NO CONFLICT
	// ====================

	$.fn.strate.noConflict = function () {
		$.fn.strate = old;
		return this;
	};


	// STRATE DATA-API
	// =================

	$(document).on('click.oblique-ui.strate.data-api', selector, function(e){
		var $this   = $(this);
		var href    = null;
		var target  = $this.attr('data-target') || e.preventDefault() || (href = $this.attr('href')) && href.replace(/.*(?=#[^\s]+$)/, ''); // strip for ie7
		var $target = $(target);

		Plugin.call($target.length ? $target : $this.closest('.strate-view.in, .strate-overlay.in'), 'toggle');
	});

	$(document).keyup(function(e) {
		if (e.keyCode === 27) { // ESC
			Plugin.call($('.strate-view.in, .strate-overlay.in').last(), 'hide');
		}
	});

}(jQuery);
