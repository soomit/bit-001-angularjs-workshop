
function getMessagePromise() {
  /**
   *  Wrap api.getMessage(cb) into a promise
   *
   *  hint: use Q.defer()
   *  hint2: remember, you must return a promise
   */
};

getMessagePromise()
  .then(showMessage)
  .catch(showErrorMessage);