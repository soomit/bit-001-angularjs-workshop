/**
 * Complete the function so it returns a promise
 */

function getMessagePromise() {
  var deferred = Q.defer();

  api.getMessage(function(err, message) {
    if (err) {
      deferred.reject(err);
    } else {
      deferred.resolve(message);
    }
  });

  return deferred.promise;
};

getMessagePromise().then(showMessage, showErrorMessage);