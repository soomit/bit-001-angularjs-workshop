Exercise 2: Creating Promises
=============================

Use case: Within your application you work with promises. Unfortunately a 3rd party library has a callback based api.

Create a promise that wraps a callback function. Use the deferred syntax.