/**
 * Wait for a message AND the first click on the screen, then output the message
 * hint: use Q.all()
 */

var messageP = api.getMessage();
var clickP = api.firstClick();