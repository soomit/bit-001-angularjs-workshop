/**
 * Print the boss of the employee with the given user id. Fetch the employee first, then make a second
 * call to the employee function to get the boss. Combine both results and print the message. Use
 * the given function stubs.
 */

function combineWithBoss(employee) {
  //TODO returns { boss: ..., employee: ..}
  return Q.reject('Implement me!');
}

function createMessage(info) {
  return info.employee.firstName + ' reports to ' + info.boss.firstName;
}

// Don't modify this function
function printBoss(id) {
  api.getEmployeePromise(id)
    .then(combineWithBoss)
    .then(createMessage)
    .then(showMessage)
    .catch(showErrorMessage);
}

printBoss(1);
