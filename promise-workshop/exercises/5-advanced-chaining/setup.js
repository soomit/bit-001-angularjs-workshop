var people = {
  1: {
    firstName: 'Andrew',
    lastName: 'Potts',
    boss: 2
  },
  2: {
    firstName: 'Andy',
    lastName: 'Lottman',
    boss: 3
  },
  3: {
    firstName: 'Big',
    lastName: 'Cheese',
    boss: 4
  }
};

var api = {
  getEmployeePromise: function(id) {
    var person = people[id];
    if (person) {
      return mockPromise(person, 500);
    } else {
      return mockPromiseReject('Employee not found', 500);
    }
  }
};
