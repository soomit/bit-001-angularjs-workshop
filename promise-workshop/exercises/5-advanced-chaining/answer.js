
function combineWithBoss(employee) {
  return api.getEmployeePromise(employee.boss)
    .then(function(boss) {
      return {
        boss: boss,
        employee: employee
      };
    });
}

function createMessage(info) {
  return info.employee.firstName + ' reports to ' + info.boss.firstName;
}

function printBoss(id) {
  api.getEmployeePromise(id)
    .then(combineWithBoss)
    .then(createMessage)
    .then(showMessage)
    .catch(showErrorMessage);
}

printBoss(1);
