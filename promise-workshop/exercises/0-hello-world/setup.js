var api = {
  getName: function(cb) {
  	setTimeout(function() {
      cb(null, 'Vinay');
    }, 500);
  },

  /**
   * Returns a promise that resolves a name after half a second
   * @returns {object} - Promise that resolves a name
   */
  getNamePromise: function() {
    return mockPromise('Vinay', 500);
  }
};