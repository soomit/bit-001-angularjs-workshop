/**
 * Use promises instead of callbacks.
 *
 * Hint: use api.getNamePromise()
 *
 * Checkout setup.js for the available API
 */
api.getName(function(err, name) {
  if (!err) {
    showMessage('Hello, ' + name + '!');
  }
});
