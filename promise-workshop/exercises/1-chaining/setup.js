var person = {
  firstName: 'Derek',
  lastName: 'Brown',
  favoriteTeam: 'Eagles'
};

var api = {

  /**
   * Get person (callback)
   */

  getPerson: function(cb) {
  	setTimeout(function() {
      cb(null, person);
    }, 500);
  },

  /**
   * Get person (promise)
   */
  getPersonPromise: function() {
    return mockPromise(person, 500);
  },

  /**
   * Get message from person (callback)
   */
  getMessage: function(person, cb) {
    setTimeout(function() {
      cb(null, person.firstName + ' ' + person.lastName + ' likes the ' + person.favoriteTeam);
    }, 500);
  },

  /**
   * Get message from person (promise)
   */
  getMessagePromise: function(person) {
    return mockPromise(person.firstName + ' ' + person.lastName + ' likes the ' + person.favoriteTeam, 500);
  },

  mapMessage: function(message) {
    return message + ', the 2nd worst team in the NFC east.';
  }
};
