/**
 * Fetch the person, fetch the message, map the message, print the message
 *
 * Exercise: Do the same thing using promises
 *
 * Hint: see setup.js
 */

api.getPerson(function(err, person) {
  api.getMessage(person, function(err, message) {
    showMessage(api.mapMessage(message));
  });
});
