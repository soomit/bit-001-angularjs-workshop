/**
 * Print out all the error message
 * http://documentup.com/kriskowal/q/#handling-errors
 */

api.getMessage('a')
  .then(api.throwError, appendErrorMessage);

api.getMessage('b')
  .then(api.causeError, appendErrorMessage);

api.getMessage('c')
  .then(api.returnError, appendErrorMessage);