/**
 * Print out all the error messages
 */

api.getMessage('a')
  .then(api.throwError, appendErrorMessage)
  .catch(appendErrorMessage);

api.getMessage('b')
  .then(api.causeError, appendErrorMessage)
  .catch(appendErrorMessage);

api.getMessage('c')
  .then(api.returnError, appendErrorMessage)
  .catch(appendErrorMessage);

// api.getMessage('a')
//   .then(function(msg) {
//     appendMessage(msg);  
//   })
//   .then(api.throwError)
//   .then(function() {
//     return api.getMessage('b');
//   })
//   .then(function(msg) {
//     appendMessage(msg);  
//   })
//   .catch(appendErrorMessage)