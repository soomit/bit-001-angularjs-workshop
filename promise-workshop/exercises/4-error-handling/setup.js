var api = {

  getMessage: function(msg) {
    return Q('everything is fine ' + msg + '<br>');
  },

  /**
   * Throws a JS error
   */
  throwError: function() {
    throw 'I threw an error!';
  },

  /**
   * Causes a JS error
   */
  causeError: function() {
    BIT++;
  },

  /**
   * Returns a rejected promise
   */
  returnError: function() {
    return Q.reject('A rejected promise');
  }
};
